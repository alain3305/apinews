library(plumber)
library(uuid)
library(rlist)

apinews.globals <- new.env()
apinews.globals$GLOBAL_SESSION_LIST <- list()

#' Start the REST API
#'
#' @import plumber
#' @importFrom uuid UUIDgenerate
#' @importFrom rlist list.append
#' @importFrom rlist list.remove
#'
#' @param get_headlines_function_name A function with no parameter, returning a data frame, with at least two string columns: article_uid and article_title
#' @param get_article_fonction_name A function with two expected parameters: article_uid, the ID of the target article, headlines, the data frame returned by the first function
#' @param api_port the port for the API
#'
#' @return session logs
#' @export
#'
#' @examples
#' start_api(my_headlines_function, my_article_function)
start_api <- function(get_headlines_function_name, get_article_fonction_name, api_port = 8000) {

  ### TEST DE LA VALIDITE DES PARAMETRES ###
  if( exists(get_headlines_function_name) == FALSE ) {
    stop("The function with name given by get_headlines_function_name does not exist.")
  }
  if( exists(get_article_fonction_name) == FALSE ) {
    stop("The function with name given by get_article_fonction_name does not exist.")
  }
  if( is.null(names(formals(get_headlines_function_name))) == FALSE ) {
    stop("The function with name given by get_headlines_function_name should not require any argument.")
  }
  if( identical(names(formals(get_article_fonction_name)), c("article_uid", "headlines")) == FALSE ) {
    stop("The function with name given by get_headlines_function_name should require only article_uid and headlines arguments")
  }

  ### CREATION DU WEBSERVICE ###
  pr <- plumber$new()

  ### FONCTION DE CREATION DE SESSION UTILISATEUR ###
  pr$handle("GET", "/open", function(req, res) {
    session_uuid <- UUIDgenerate(use.time = FALSE)
    if( session_uuid %in% sapply(apinews.globals$GLOBAL_SESSION_LIST, function(x) { x$session_uuid }) == TRUE ) {
      stop("Error in UUID gezneration : duplicated UUID.")
    }
    apinews.globals$GLOBAL_SESSION_LIST <- list.append(apinews.globals$GLOBAL_SESSION_LIST, list(session_uuid = session_uuid,
                                                                  headlines_df = NULL))
    return(session_uuid = session_uuid)
  })

  ### FONCTION DE FERMETURE DE SESSION UTILISATEUR ###
  pr$handle("GET", "/close/session/<session>", function(req, res, session) {
    if( session %in% sapply(apinews.globals$GLOBAL_SESSION_LIST, function(x) { x$session_uuid }) == FALSE ) {
      stop("The UUID ", session, " does not exist.")
    }
    # voir si pas de sécurité possible pour éviter suppression par un tiers
    session_index <- which(sapply(apinews.globals$GLOBAL_SESSION_LIST, function(x) { identical(x$session_uuid, session) }))
    apinews.globals$GLOBAL_SESSION_LIST <- list.remove(apinews.globals$GLOBAL_SESSION_LIST, session_index)
    return(session_closed = session)
  })

  ### FONCTION DE LISTING DES CONNEXIONS ACTIVES ###
  # uniquement pour le debug
  pr$handle("GET", "/get_active_sessions", function(req, res) {
    return(sessions = sapply(apinews.globals$GLOBAL_SESSION_LIST, function(x) { x$session_uuid }))
  })

  ### FONTION DE RECUPERATION DE LA LISTE DES ARTICLES ###
  pr$handle("GET", "/headlines/session/<session>", function(req, res, session) {
    if( session %in% sapply(apinews.globals$GLOBAL_SESSION_LIST, function(x) { x$session_uuid }) == FALSE ) {
      stop("The UUID ", session, " does not exist.")
    }
    session_index <- which(sapply(apinews.globals$GLOBAL_SESSION_LIST, function(x) { identical(x$session_uuid, session) }))
    result <- do.call(get_headlines_function_name, list())

    # Verification de la presence des variables obligatoires
    if( ("article_uid" %in% names(result)) == FALSE ) {
      stop("The result must include a column named article_uid")
    }
    if( ("title" %in% names(result)) == FALSE ) {
      stop("The result must include a column named title")
    }
    if( ("pubdate" %in% names(result)) == FALSE ) {
      stop("The result must include a column named title")
    }

    # Verficiation du type des variables obligatoires
    if( ("character" %in% class(result$article_uid)) == FALSE ) {
      stop("The article_uid must be of class character")
    }
    if( ("character" %in% class(result$title)) == FALSE ) {
      stop("The title must be of class character")
    }
    if( ("Date" %in% class(result$pubdate)) == FALSE ) {
      stop("The pubdate must be of class Date")
    }

    # Verification de l'absence de donnees manquantes dans les variables obligatoires
    if( any(result$article_uid == "") ) {
      stop("The column article_uid must not contain empty values.")
    }
    if( any(result$title == "") ) {
      stop("The column title must not contain empty values.")
    }
    if( any(result$date == "") ) {
      stop("The column title must not contain empty values.")
    }

    apinews.globals$GLOBAL_SESSION_LIST[[session_index]]$headlines_df <- result
    return(result)
  })

  ### FONCTION DE RECUPERATION DU CONTENU D'UN ARTICLE ###
  pr$handle("GET", "/article/session/<session>/article_uid/<article_uid>", function(req, res, session, article_uid) {
    if( session %in% sapply(apinews.globals$GLOBAL_SESSION_LIST, function(x) { x$session_uuid }) == FALSE ) {
      stop("The UUID ", session, " does not exist.")
    }
    session_index <- which(sapply(apinews.globals$GLOBAL_SESSION_LIST, function(x) { identical(x$session_uuid, session) }))
    if( (article_uid %in% apinews.globals$GLOBAL_SESSION_LIST[[session_index]]$headlines_df$article_uid) == FALSE ) {
      stop("The article ", article_uid, " does not exist in current headlines.")
    }

    session_headlines <- apinews.globals$GLOBAL_SESSION_LIST[[session_index]]$headlines_df
    result <- do.call(get_article_fonction_name, list(article_uid = article_uid, headlines = session_headlines))

    if( ("content" %in% names(result)) == FALSE ) {
      stop("The result must include a column named content")
    }
    if( ("character" %in% class(result$content)) == FALSE ) {
      stop("The content must be of class character")
    }

    return(result)
  })

  ### ACTIVAYION DU WEBSERVICE REST ###
  pr$run(host = "0.0.0.0", port = api_port, swagger = FALSE)

}
